﻿using ConsoleAdoInvoice.Bill;
using ConsoleAdoInvoice.BillService;
using ConsoleAdoInvoice.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdoInvoice.Controllers
{
    public class WCFInvoiceController
    {

        internal static void ListInvoice(Service1Client proxy)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = proxy.GetAllInvoice();
            stopWatch.Stop();
            foreach (var invoice in result)
            {
                Console.WriteLine(
                    invoice.Id + " "
                    + invoice.Created.ToString("yyyy-mm-dd") +" "
                    + invoice.InvoiceNr + "\t"
                    + invoice.OrderNr +"\t"
                    + invoice.Fullname +"\t"
                    + invoice.Address +"\t"
                    + invoice.City +"\t"
                    );
            }

            Console.WriteLine("Det tog " + stopWatch.ElapsedMilliseconds + "att hämta listan");
        }

        internal static void CreateInvoice(Service1Client proxy)
        {
            var stopWatch = new Stopwatch();
            var currentInvoiceNr = 1;
            try
            {
                currentInvoiceNr = proxy.GetAllInvoice().Max(p => p.InvoiceNr);
                currentInvoiceNr++;
            }
            catch (Exception)
            {
                Console.WriteLine("Inga fakturor är skapade");
            }

            Console.WriteLine("Senaste fakturanumret: " + (currentInvoiceNr - 1));

            var listOfOrders = proxy.GetListOfAllOrders().GroupBy(p => p.OrderNr).Select(q => q.First()).Where(r => r.Status == 0);
            Console.WriteLine("Antal ej fakturerade ordrar: " + listOfOrders.Count());

            if (listOfOrders.Count() != 0)
            {
                Console.Write("Skapa ordrar tryck enter");
                Console.ReadLine();

                var listofOrder = proxy.GetListOfAllOrders();
                
                try
                {
                    bool status = true;
                    
                    stopWatch.Start();
                    foreach (var item in listOfOrders)
                    {
                        var customer = proxy.GetAllCustomers().FirstOrDefault(u => u.Id == item.CustomerId);
                        var newInvoice = new InvoiceModel
                        {
                            Fullname = customer.FirstName + " " + customer.LastName,
                            Address = customer.Address,
                            City = customer.City,
                            OrderNr = item.OrderNr,
                            InvoiceNr = ++currentInvoiceNr,
                        };

                        proxy.CreateInvoice(newInvoice);
                    }
                    stopWatch.Stop();
                    
                    proxy.UpdateOrderStatus(status);
                    Console.WriteLine("Skapade fakturor på: " + stopWatch.ElapsedMilliseconds + " ms");


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Det finns inga fakturor att skapa");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}
