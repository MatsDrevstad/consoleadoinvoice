﻿using ConsoleAdoInvoice.Bill;
using ConsoleAdoInvoice.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ConsoleAdoInvoice.BillService;

namespace ConsoleAdoInvoice.Controllers
{
    public class WCFOrderProductController
    {

        internal static void ListOrderProducts(Service1Client proxy)
        {
            var result = proxy.GetListOfAllOrders();

            Console.WriteLine(
                "Id\tStatus\tCuId\tOrderNr\tQty\tUnitPrice\tProductName"
                );

            foreach (var order in result)
            {
                Console.WriteLine(
                    order.Id + "\t" +
                    order.Status + "\t" +
                    order.CustomerId + "\t" +
                    order.OrderNr + "\t" +
                    order.Qty + "\t" +
                    order.UnitPrice.ToString("c") + "\t" +
                    order.ProductName
                    );
            }
        }

        internal static void AddOrderProduct(Service1Client proxy)
        {
            bool loop = true;
            while (loop) { loop = AddOrderProductProcess(proxy); }
        }

        private static bool AddOrderProductProcess(Service1Client proxy)
        {
            Console.Write("Ange kundnummer: ");
            var customerNumber = int.Parse(Console.ReadLine());
            var currentCustomer = proxy.GetCustomer(customerNumber);

            if (currentCustomer != null)
            {
                Console.WriteLine();
                Console.WriteLine(currentCustomer.FirstName + " " + currentCustomer.LastName);
                Console.WriteLine(currentCustomer.Address + " " + currentCustomer.City);
                Console.WriteLine();                
            }
            else
            {
                Console.WriteLine("Ingen kund matchar kundnumret");
                return false;
            }

            Console.Write("Ange Produktnummer: ");
            var productNumber = int.Parse(Console.ReadLine());
            var currentProdukt = proxy.GetProdoct(productNumber);

            if (currentProdukt != null)
            {
                Console.WriteLine();
                Console.WriteLine(currentProdukt.ProductName);
                Console.WriteLine(currentProdukt.Description);
                Console.WriteLine(currentProdukt.Price);
                Console.WriteLine();     
            }
            else
            {
                Console.WriteLine("Ingen produkt matchar");
                return false;
            }
            Console.Write("Ange antal: ");

            var quantityInput = int.Parse(Console.ReadLine());

            int currentOrderNrCount = 1;
            currentOrderNrCount = proxy.GetListOfAllOrders().Max(x => x.OrderNr);

            var newOrder = new OrderProductModel 
            {
                CustomerId = customerNumber,
                OrderNr = ++currentOrderNrCount,
                ProductName = currentProdukt.ProductName,
                Qty = quantityInput,
                Status = 0,
                UnitPrice = currentProdukt.Price,
            };

            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                proxy.AddOrderProduct(newOrder);
                stopWatch.Stop();
                Console.WriteLine("La till en ny order.. Tog " + stopWatch.ElapsedMilliseconds + "ms");
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
    }
}
