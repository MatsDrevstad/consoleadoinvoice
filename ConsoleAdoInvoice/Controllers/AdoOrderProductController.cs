﻿using ConsoleAdoInvoice.Bill;
using ConsoleAdoInvoice.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleAdoInvoice.Controllers
{
    public class AdoOrderProductController
    {
        internal static void ListOrderProducts()
        {
            var proxy = new DbProxy();
            var result = proxy.GetAll<OrderProduct>();

            Console.WriteLine(
                "Id\tStatus\tCuId\tOrderNr\tQty\tUnitPrice\tProductName"
                );

            foreach (var order in result)
            {
                Console.WriteLine(
                    order.Id + "\t" +
                    order.Status + "\t" +
                    order.CustomerId + "\t" +
                    order.OrderNr + "\t" +
                    order.Qty + "\t" +
                    order.UnitPrice.ToString("c") + "\t" +
                    order.ProductName
                    );
            }
        }

        internal static void AddOrderProduct()
        {
            bool loop = true;
            while (loop) { loop = AddOrderProductProcess(); }
        }

        private static bool AddOrderProductProcess()
        {
            Console.Write("Ange kundnummer: ");
            var customerIdInput = Console.ReadLine();

            if (Regex.IsMatch(customerIdInput, @"^\d+$"))
            {
                var proxy = new DbProxy();
                proxy.Open();

                try
                {
                    string sqlText = "SELECT * FROM Customer WHERE id = " + customerIdInput;

                    using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                    {
                        SqlDataReader rs = command.ExecuteReader();
                        if (rs.HasRows)
                        {
                            while (rs.Read())
                            {
                                Console.WriteLine(rs["FirstName"] + " " + rs["LastName"]);
                                Console.WriteLine(rs["Address"] + " " + rs["City"]);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Ingen kund match");
                            return false;
                        }
                        rs.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return true;
                }
                finally 
                {
                    proxy.Close();
                }
            }
            else
            {
                return true;
            }

            Console.Write("Ange Produktnummer: ");
            var productIdInput = Console.ReadLine();
            string productName = string.Empty;
            string price = string.Empty;

            if (Regex.IsMatch(customerIdInput, @"^\d+$"))
            {
                var proxy = new DbProxy();
                proxy.Open();

                try
                {
                    string sqlText = "SELECT * FROM Product WHERE id = " + productIdInput;

                    using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                    {
                        SqlDataReader rs = command.ExecuteReader();
                        if (rs.HasRows)
                        {
                            while (rs.Read())
                            {
                                productName = rs["ProductName"].ToString();
                                price = decimal.Parse(rs["Price"].ToString()).ToString("0.00");
                            }
                            Console.WriteLine(productName);
                            Console.WriteLine(price);
                        }
                        else
                        {
                            Console.WriteLine("Ingen produkt match");
                        }
                        rs.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return true;
                }
                finally
                {
                    proxy.Close();
                }
            }
            else
            {
                return true;
            }

            Console.Write("Ange antal: ");
            var quantityInput = Console.ReadLine();
            int orderNr = 0;

            if (Regex.IsMatch(quantityInput, @"^\d+$"))
            {

                var proxy = new DbProxy();
                proxy.Open();

                try
                {
                    string sqlText = "SELECT MAX(OrderNr) AS MaxOrderNr FROM OrderProduct";

                    using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                    {
                        SqlDataReader rs = command.ExecuteReader();
                        if (rs.Read())
                        {
                            int.TryParse(rs["MaxOrderNr"].ToString(), out orderNr);
                        }
                        rs.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return true;
                }
                finally
                {
                    proxy.Close();
                }
                orderNr++;

                var stopWatch = new Stopwatch();
                stopWatch.Start();

                proxy.Open();

                try
                {
                    string sqlText = @"INSERT INTO OrderProduct (CustomerId, OrderNr, ProductName, Qty, UnitPrice) 
                        VALUES (@CustomerId, @OrderNr, @ProductName, @Qty, @UnitPrice)";

                    using (SqlCommand command2 = new SqlCommand(sqlText, proxy.GetConnection()))
                    {
                        var customerIdParam = command2.Parameters.Add("@CustomerId", SqlDbType.Int);
                        var orderNrParam = command2.Parameters.Add("@OrderNr", SqlDbType.Int);
                        var productNameParam = command2.Parameters.Add("@ProductName", SqlDbType.NVarChar);
                        var qtyParam = command2.Parameters.Add("@Qty", SqlDbType.Int);
                        var unitPriceParam = command2.Parameters.Add("@UnitPrice", SqlDbType.Money);
                        customerIdParam.Value = customerIdInput;
                        orderNrParam.Value = orderNr;
                        productNameParam.Value = productName;
                        qtyParam.Value = int.Parse(quantityInput);
                        unitPriceParam.Value = price;
                        command2.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return true;
                }
                finally
                {
                    proxy.Close();
                }
                var message = "Tid: " + stopWatch.ElapsedMilliseconds + "ms";
                stopWatch.Stop();
                Console.WriteLine(message);
            }
            else
            {
                return true;
            }
            return false;
        }
    }
}
