﻿using ConsoleAdoInvoice.Bill;
using ConsoleAdoInvoice.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdoInvoice.Controllers
{
    public class AdoInvoiceController
    {

        internal static void ListInvoice()
        {
            var proxy = new DbProxy();
            var result = proxy.GetAll<Invoice>();

            foreach (var invoice in result)
            {
                Console.WriteLine(
                    invoice.Id + " "
                    + invoice.Created.ToString("yyyy-mm-dd") +" "
                    + invoice.InvoiceNr + "\t"
                    + invoice.OrderNr +"\t"
                    + invoice.Fullname +"\t"
                    + invoice.Address +"\t"
                    + invoice.City +"\t"
                    );
            }
        }

        internal static void CreateInvoice()
        {
            var proxy = new DbProxy();

            var currentInvoiceNr = 1;
            try
            {
                currentInvoiceNr = proxy.GetAll<Invoice>().Max(p => p.InvoiceNr);
                currentInvoiceNr++;
            }
            catch { }
            Console.WriteLine("Current Invoice nr: " + (currentInvoiceNr - 1));

            proxy.Close();

            var result = proxy.GetAll<OrderProduct>().GroupBy(p => p.OrderNr).Select(q => q.First()).Where(r => r.Status == 0);
            Console.WriteLine("Nr of unmade invoices: " + result.Count());

            proxy.Open();
            var rawInvoice = new List<Invoice>();

            try
            {
                string sqlText = @"
                    SELECT op.Id AS opid, op.*, cu.*
                    FROM OrderProduct op
                    INNER JOIN Customer cu
                    ON op.CustomerId = cu.Id
                    WHERE op.Status = 0
                    ";

                using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                {
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        rawInvoice.Add(new Invoice
                        {
                            InvoiceNr = 0, // currentInvoiceNr++ sätts senare pga att vi kör en group på denna lista
                            OrderNr = (int)dataReader["OrderNr"],
                            Fullname = (string)dataReader["FirstName"] + " " + (string)dataReader["LastName"],
                            Address = (string)dataReader["Address"],
                            City = (string)dataReader["City"],
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }

            var listInvoice = new List<Invoice>();
            foreach (var item in rawInvoice.GroupBy(p => p.OrderNr).Select(q => q.First()))
            {
                listInvoice.Add(item);
                item.InvoiceNr = currentInvoiceNr++;
            }

            int bulksize = 10000;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            proxy.Open();
            try
            {
                var bulkCopy = new SqlBulkCopy(proxy.GetConnection());
                bulkCopy.DestinationTableName = "Invoice";
                bulkCopy.ColumnMappings.Add("InvoiceNr", "InvoiceNr");
                bulkCopy.ColumnMappings.Add("OrderNr", "OrderNr");
                bulkCopy.ColumnMappings.Add("FullName", "FullName");
                bulkCopy.ColumnMappings.Add("Address", "Address");
                bulkCopy.ColumnMappings.Add("City", "City");

                do
                {
                    using (var dataReader = new ObjectDataReader<Invoice>(listInvoice.Take(bulksize)))
                    {
                        bulkCopy.WriteToServer(dataReader);
                    }
                    IEnumerable<Invoice> enumListOrder = listInvoice;
                    listInvoice = enumListOrder.Skip(bulksize).ToList();

                } while (listInvoice.Count() > 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Insert successfully in {0}ms", stopwatch.ElapsedMilliseconds);
            Console.ForegroundColor = ConsoleColor.Gray;
            stopwatch.Stop();

            proxy.Open();
            try
            {
                string sqlText = "UPDATE OrderProduct SET Status = 1 where Status = 0";

                using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
        }
        

        internal static void CreateInvoiceOld()
        {
            var proxy = new DbProxy();

            var currentInvoiceNr = 1;
            try
            {
                currentInvoiceNr = proxy.GetAll<Invoice>().Max(p => p.InvoiceNr);
                currentInvoiceNr++;
            }
            catch { }
            Console.WriteLine("Senaste fakturanummer: " + (currentInvoiceNr - 1));

            proxy.Close();

            var result = proxy.GetAll<OrderProduct>().GroupBy(p => p.OrderNr).Select(q => q.First()).Where(r => r.Status == 0);
            Console.WriteLine("Antal ej fakturerade ordrar: " + result.Count());

            proxy.Open();
            var listOrders = new List<OrderProductCustomer>();

            try
            {
                string sqlText = @"
                    SELECT op.Id AS opid, op.*, cu.*
                    FROM OrderProduct op
                    INNER JOIN Customer cu
                    ON op.CustomerId = cu.Id
                    WHERE op.Status = 0
                    ";

                using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                {
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        listOrders.Add(new OrderProductCustomer
                        {
                            Id = (int)dataReader["opid"],
                            Status = (int)dataReader["Status"],
                            CustomerId = (int)dataReader["CustomerId"],
                            OrderNr = (int)dataReader["OrderNr"],
                            ProductName = (string)dataReader["ProductName"],
                            Qty = (int)dataReader["Qty"],
                            UnitPrice = (decimal)dataReader["UnitPrice"],
                            FirstName = (string)dataReader["FirstName"],
                            LastName = (string)dataReader["LastName"],
                            Address = (string)dataReader["Address"],
                            City = (string)dataReader["City"],
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
           
            foreach (var item in listOrders.GroupBy(p => p.OrderNr).Select(q => q.First()))
            {
                proxy.Open();
                try
                {
                    string sqlText = @"INSERT INTO Invoice (InvoiceNr, OrderNr, Fullname, Address, City) 
                        VALUES (@InvoiceNr, @OrderNr, @Fullname, @Address, @City)";

                    using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                    {
                        var invParam = command.Parameters.Add("InvoiceNr", SqlDbType.Int);
                        var ordParam = command.Parameters.Add("OrderNr", SqlDbType.Int);
                        var namParam = command.Parameters.Add("Fullname", SqlDbType.NVarChar);
                        var adrParam = command.Parameters.Add("Address", SqlDbType.NVarChar);
                        var ctyParam = command.Parameters.Add("City", SqlDbType.NVarChar);
                        invParam.Value = currentInvoiceNr++;
                        ordParam.Value = item.OrderNr;
                        namParam.Value = item.FirstName + " " + item.LastName;
                        adrParam.Value = item.Address;
                        ctyParam.Value = item.City;
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    proxy.Close();
                }

                proxy.Open();
                try
                {
                    string sqlText = "UPDATE OrderProduct SET Status = 1 where Status = 0";

                    using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    proxy.Close();
                }
            }
        }
    }
}
