﻿using ConsoleAdoInvoice.Bill;
using ConsoleAdoInvoice.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ConsoleAdoInvoice.BillService;

namespace ConsoleAdoInvoice.Controllers
{
    public class WCFProductsController
    {
        internal static void ListProducts(Service1Client proxy)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = proxy.GetAllProducts();
            var message = "Tid: " + stopWatch.ElapsedMilliseconds + " ms";
            stopWatch.Stop();

            
            foreach (var product in result)
            {
                Console.WriteLine(
                    product.Id + "\t"
                    + product.ArticleId + "\t"
                    + double.Parse(product.Price.ToString()).ToString("c") + "\t"
                    + product.ProductName
                    );
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(
            "Id\tArt nr\tUnitPrice\tProductName"
            );

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        internal static void AddProduct(Service1Client proxy)
        {
            Console.Write("Ange namn på produkten: ");
            var nameInput = Console.ReadLine();
            Console.Write("Ange pris på produkten: ");
            var priceInput = decimal.Parse(Console.ReadLine());

            var newProduct = new ProductModel
            {
                Price = priceInput,
                ProductName = nameInput,
            };

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            proxy.AddProduct(newProduct);
            stopWatch.Stop();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Tid: " + stopWatch.ElapsedMilliseconds + "ms");
            Console.ForegroundColor = ConsoleColor.Gray;
        }


        internal static void DeleteProduct(Service1Client proxy)
        {
            var stopWatch = new Stopwatch();
            var allProduct = proxy.GetAllProducts();
            foreach (var product in allProduct)
            {
                Console.WriteLine(
                    product.Id + "\t"
                    + product.ArticleId + "\t"
                    + double.Parse(product.Price.ToString()).ToString("c") + "\t"
                    + product.ProductName
                    );
            }

            Console.Write("Ange id på produkten som skall tas bort: ");
            var idInput = int.Parse(Console.ReadLine());


            stopWatch.Start();
            proxy.DeleteProduct(idInput);
            stopWatch.Stop();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Tid: " + stopWatch.ElapsedMilliseconds + "ms");
            Console.ForegroundColor = ConsoleColor.Gray;

        }

        internal static void UpdateProduct(Service1Client proxy)
        {
            var stopWatch = new Stopwatch();
            Console.Write("Ange id på produkten som skall ändras: ");
            var idInput = int.Parse(Console.ReadLine());

            var product = proxy.GetProdoct(idInput);

            Console.WriteLine(
                product.Id + "\t"
                + product.ArticleId + "\t"
                + double.Parse(product.Price.ToString()).ToString("c") + "\t"
                + product.ProductName
                );

            Console.Write("Nytt produktnamn: ");
            var newProductName = Console.ReadLine();
            Console.Write("Nytt Pris: ");
            decimal newPrice = decimal.Parse(Console.ReadLine());

            var newProduct = new ProductModel
            {
                Id = idInput,
                ProductName = newProductName,
                Price = newPrice,
            };


            stopWatch.Start();
            proxy.UpdateProduct(newProduct);
            stopWatch.Stop();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Tid: " + stopWatch.ElapsedMilliseconds + "ms");
            Console.ForegroundColor = ConsoleColor.Gray;

        }

        internal static void UpdatePriceDB(Service1Client proxy)
        {

            Console.WriteLine("Med hur mycket vill du ändra priserna på alla produkter?");
            Console.WriteLine("Skriv så mycket som det nuvarande priset bör multipliceras med ( använd komma )");
            Console.Write("Input: ");
            var value = decimal.Parse(Console.ReadLine());

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            proxy.UpdateAllPrices(value);
            stopWatch.Stop();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Att ändra ALLA priser i databasen tog: " + stopWatch.ElapsedMilliseconds + " ms");
            Console.ForegroundColor = ConsoleColor.Gray;

        }
    }
}

