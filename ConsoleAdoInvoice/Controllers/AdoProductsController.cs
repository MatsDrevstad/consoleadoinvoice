﻿using ConsoleAdoInvoice.Bill;
using ConsoleAdoInvoice.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleAdoInvoice.Controllers
{
    public class AdoProductsController
    {
        internal static void ListProducts()
        {
            var proxy = new DbProxy();

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = proxy.GetAll<Product>();
            var message = "Tid: " + stopWatch.ElapsedMilliseconds + " ms";
            stopWatch.Stop();

            foreach (var product in result)
            {
                Console.WriteLine(
                    product.Id + "\t"
                    + product.ArticleId + "\t"
                    + double.Parse(product.Price.ToString()).ToString("c") + "\t"
                    + product.ProductName
                    );
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        internal static void ListProductsVy()
        {
            var proxy = new DbProxy();

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = proxy.GetAll<Product>("SelectAllProduct");
            var message = "Tid: " + stopWatch.ElapsedMilliseconds + "ms";
            stopWatch.Stop();

            foreach (var product in result)
            {
                Console.WriteLine(
                    product.Id + "\t"
                    + product.ArticleId + "\t"
                    + double.Parse(product.Price.ToString()).ToString("c") + "\t"
                    + product.ProductName
                    );
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }


        internal static void AddProduct()
        {
            Console.Write("Ange namn på produkten: ");
            var nameInput = Console.ReadLine();
            Console.Write("Ange pris på produkten: ");
            var priceInput = Console.ReadLine();

            var proxy = new DbProxy();

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            proxy.Open();

            try
            {
                string sqlText = "INSERT INTO Product (ProductName, Price) VALUES (@ProductName, @Price)";

                using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                {
                    var nameParam = command.Parameters.Add("ProductName", SqlDbType.NVarChar);
                    var priceParam = command.Parameters.Add("Price", SqlDbType.Money);
                    nameParam.Value = nameInput;
                    priceParam.Value = priceInput;
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
            var message = "Tid: " + stopWatch.ElapsedMilliseconds + "ms";
            stopWatch.Stop();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        internal static void AddProductSP()
        {

            Console.Write("Ange namn på produkten: ");
            var nameInput = Console.ReadLine();
            Console.Write("Ange pris på produkten: ");
            var priceInput = Console.ReadLine();

            var proxy = new DbProxy();

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            proxy.Open();

            try
            {
                using (SqlCommand command = new SqlCommand("spAddProduct", proxy.GetConnection()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("ProductName", nameInput);
                    command.Parameters.AddWithValue("Price", priceInput);

                    SqlParameter outPutParameter = new SqlParameter();
                    outPutParameter.ParameterName = "@Id";
                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;
                    outPutParameter.Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add(outPutParameter);
                    command.ExecuteNonQuery();
                    string Id = outPutParameter.Value.ToString();
                    Console.WriteLine("Sparat ID: {0}", Id);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                proxy.Close();
            }
            var message = "Tid: " + stopWatch.ElapsedMilliseconds + "ms";
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;

        }


        internal static void DeleteProduct()
        {
            Console.Write("Ange id på produkten som skall tas bort eller godtycklig bokstav för ångra: ");
            var idInput = Console.ReadLine();

            if (Regex.IsMatch(idInput, @"^\d+$"))
            {
                var proxy = new DbProxy();

                var stopWatch = new Stopwatch();
                stopWatch.Start();

                proxy.Open();

                string sqlText = "DELETE FROM Product WHERE Id = " + idInput;

                SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection());
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    proxy.Close();
                }

                var message = "Tid: " + stopWatch.ElapsedMilliseconds + "ms";
                stopWatch.Stop();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine();
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        internal static void UpdateProduct()
        {


            Console.Write("Ange id på produkten som skall ändras eller godtycklig bokstav för ångra: ");
            var idInput = Console.ReadLine();
            string price = string.Empty;

            if (Regex.IsMatch(idInput, @"^\d+$"))
            {
                var proxy = new DbProxy();

                try
                {
                    price = decimal.Parse(proxy.GetById<Product>(idInput).ToList()[0].Price.ToString()).ToString("0.00");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.Write("Ange nytt pris ({0}): ", price);
                price = Console.ReadLine();

                var stopWatch = new Stopwatch();
                stopWatch.Start();

                proxy.Open();
                try
                {
                    string sqlText = "UPDATE Product SET Price = @NewPrice WHERE Id = @InputId";

                    using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                    {
                        command.Parameters.AddWithValue("@NewPrice", price.Trim());
                        command.Parameters.AddWithValue("@InputId", idInput);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    proxy.Close();
                }

                var message = "Tid: " + stopWatch.ElapsedMilliseconds + "ms";
                stopWatch.Stop();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine();
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.Gray;

            }
        }

        internal static void UpdatePriceDB()
        {
            var proxy = new DbProxy();
            
            Console.WriteLine("Med hur mycket vill du ändra priserna på alla produkter?");
            Console.WriteLine("Skriv så mycket som det nuvarande priset bör multipliceras med ( använd punkt )");
            Console.Write("Input: ");
            string value = Console.ReadLine();

            var stopWatch = new Stopwatch();

            proxy.Open();
            try
            {
                var sqlText = "UPDATE Product SET Price = Price * @Value";

                stopWatch.Start();
                using (SqlCommand command = new SqlCommand(sqlText, proxy.GetConnection()))
                {
                    command.Parameters.AddWithValue("@Value", value);
                    command.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                proxy.Close();
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Att ändra ALLA priser i databasen tog: " + stopWatch.ElapsedMilliseconds + " ms");
            Console.ForegroundColor = ConsoleColor.Gray;
            stopWatch.Stop();
        }
    }
}

