﻿using ConsoleAdoInvoice.Bill;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdoInvoice.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public int InvoiceNr { get; set; }
        public int OrderNr { get; set; }
        public string Fullname { get; set; }
        public string Address { get; set; }
        public string City { get; set; }

    }
}
