﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdoInvoice.Models
{
    public class OrderProduct
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public int CustomerId { get; set; }
        public int OrderNr { get; set; }
        public string ProductName { get; set; }
        public int Qty { get; set; }
        public decimal UnitPrice { get; set; }
    }

    public class OrderProductCustomer
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public int CustomerId { get; set; }
        public int OrderNr { get; set; }
        public string ProductName { get; set; }
        public int Qty { get; set; }
        public decimal UnitPrice { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
    }

}
