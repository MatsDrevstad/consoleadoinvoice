﻿using ConsoleAdoInvoice.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConsoleAdoInvoice.BillService;

namespace ConsoleAdoInvoice
{
    class InvoiceApplication
    {
        private Service1Client proxy;

        public InvoiceApplication()
        {
            Console.WindowHeight = 60;
            Console.WindowWidth = 100;
            proxy = new Service1Client();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Välkommen till Console ADO Invoice");
            Console.ForegroundColor = ConsoleColor.Gray;

            bool loop = true;
            while (loop)
            {
                PrintMenuOptions();
                loop = HandleMenuOptions(loop);
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Välkommen tillbaka");
            Thread.Sleep(500);
        }

        public bool HandleMenuOptions(bool loop)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Input:\t");
            string input = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Gray;

            if (input.Equals("1")) { AdoProductsController.ListProducts(); }
            if (input.Equals("2")) { AdoProductsController.ListProductsVy(); }
            if (input.Equals("3")) { AdoProductsController.AddProduct(); }
            if (input.Equals("4")) { AdoProductsController.AddProductSP(); }
            if (input.Equals("5")) { AdoProductsController.DeleteProduct(); }
            if (input.Equals("6")) { AdoProductsController.UpdateProduct(); }
            if (input.Equals("7")) { AdoProductsController.UpdatePriceDB(); }
            if (input.Equals("8")) { AdoOrderProductController.AddOrderProduct(); }
            if (input.Equals("9")) { AdoOrderProductController.ListOrderProducts(); }
            if (input.Equals("10")) { AdoInvoiceController.ListInvoice(); }
            if (input.Equals("11")) { AdoInvoiceController.CreateInvoice(); }

            if (input.Equals("12")) { WCFProductsController.ListProducts(proxy); }
            if (input.Equals("13")) { WCFProductsController.AddProduct(proxy); }
            if (input.Equals("14")) { WCFProductsController.DeleteProduct(proxy); }
            if (input.Equals("15")) { WCFProductsController.UpdateProduct(proxy); }
            if (input.Equals("16")) { WCFProductsController.UpdatePriceDB(proxy); }
            if (input.Equals("17")) { WCFOrderProductController.AddOrderProduct(proxy); }
            if (input.Equals("18")) { WCFOrderProductController.ListOrderProducts(proxy); }
            if (input.Equals("19")) { WCFInvoiceController.ListInvoice(proxy); }
            if (input.Equals("20")) { WCFInvoiceController.CreateInvoice(proxy); }
            if (input.Equals("0")) { loop = false; }

            return loop;
        }

        private void PrintMenuOptions()
        {
            Console.WriteLine();
            Console.WriteLine("Produkt\t[1] Lista (Select i kod)");
            Console.WriteLine("\t[2] Lista (Select i Vy)");
            Console.WriteLine("\t[3] Lägg till (Insert i kod)");
            Console.WriteLine("\t[4] Lägg till (Insert i SP)");
            Console.WriteLine("\t[5] Ta bort");
            Console.WriteLine("\t[6] Ändra");
            Console.WriteLine("\t[7] Uppdatera ALLA priser");
            Console.WriteLine("Order\t[8] Lägg till köp");
            Console.WriteLine("\t[9] Lista");
            Console.WriteLine("Faktura\t[10] Lista");
            Console.WriteLine("\t[11] Skapa");
            Console.WriteLine();
            Console.WriteLine("Köra med WCF");
            Console.WriteLine("\t[12] Produkt Lista");
            Console.WriteLine("\t[13] Produkt Lägga till ");  
            Console.WriteLine("\t[14] Produkt tabort"); 
            Console.WriteLine("\t[15] Produkt Ändra"); 
            Console.WriteLine("\t[16] Priser Uppdatera ALLA"); 
            Console.WriteLine("\t[17] Order Lägga till"); 
            Console.WriteLine("\t[18] Ordrar Lista alla"); 
            Console.WriteLine("\t[19] Fakturor Lista alla"); 
            Console.WriteLine("\t[20] Fakturor Skapa"); 

            Console.WriteLine();
            Console.WriteLine("\t[0] EXIT");
            Console.WriteLine();


        }
    }
}


